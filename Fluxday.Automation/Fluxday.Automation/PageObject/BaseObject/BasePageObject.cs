﻿using OpenQA.Selenium;
using System.Linq;

namespace Fluxday.Automation.Tests.PageObject.BaseObject
{
    public class BasePageObject
    {
        protected readonly IWebDriver Driver;

        public BasePageObject(IWebDriver driver)
        {
            Driver = driver;
        }

        public bool IsElementPresent(By by)
        {
            return Driver.FindElements(by).Any();
        }

        public void FillInInput(IWebElement iWebElement, string inputText)
        {
            iWebElement.Click();
            iWebElement.Clear();
            iWebElement.SendKeys(inputText);
        }
    }
}