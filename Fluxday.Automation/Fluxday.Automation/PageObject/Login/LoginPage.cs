﻿using OpenQA.Selenium;
using Fluxday.Automation.Tests.PageObject.NavigatePanel;

namespace Fluxday.Automation.Tests.PageObject.Login
{
    public class LoginPage : BaseObject.BasePageObject
    {
        public LoginPage(IWebDriver driver) : base(driver)
        {
        }

        protected IWebElement EmailInput
        {
            get
            {
                return Driver.FindElement(By.CssSelector("#user_email"));
            }
        }

        protected IWebElement PasswordInput
        {
            get
            {
                return Driver.FindElement(By.CssSelector("#user_password"));
            }
        }

        protected IWebElement LoginButton
        {
            get
            {
                return Driver.FindElement(By.CssSelector(".btn-login"));
            }
        }

        public void Navigate(string url)
        {
            Driver.Navigate().GoToUrl(url);
        }

        public void SetEmail(string email)
        {
            FillInInput(EmailInput, email);
        }

        public void SetPassword(string password)
        {
            FillInInput(PasswordInput, password);
        }

        public void ClickOnLoginButton()
        {
            LoginButton.Click();
        }

        public NavigatePanelPage Login(string email, string password)
        {
            SetEmail(email);
            SetPassword(password);
            ClickOnLoginButton();
            return new NavigatePanelPage(Driver);
        }
    }
}
