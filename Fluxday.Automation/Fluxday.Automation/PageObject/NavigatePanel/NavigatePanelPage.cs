﻿using OpenQA.Selenium;
using Fluxday.Automation.Tests.PageObject.BaseObject;
using System.Collections.Generic;
using System.Linq;

namespace Fluxday.Automation.Tests.PageObject.NavigatePanel
{
    public class NavigatePanelPage : BasePageObject
    {
        public NavigatePanelPage(IWebDriver driver) : base(driver) { }

        protected IWebElement SearchInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='search_keyword']"));
            }
        }

        protected IWebElement CurrentUser
        {
            get
            {
                return Driver.FindElement(By.XPath("/html/body/div[2]/div[1]/ul[3]/li[1]/a"));
            }
        }

        protected IWebElement LogoutButton
        {
            get
            {
                return Driver.FindElement(By.XPath("/html/body/div[2]/div[1]/ul[3]/li[2]/a"));
            }
        }

        protected IWebElement CreateNewTaskButton
        {
            get
            {
                return Driver.FindElement(By.XPath("/html/body/div[1]/nav/section/ul[1]/li"));
            }
        }

        protected IReadOnlyCollection<IWebElement> SectionList
        {
            get
            {
                return Driver.FindElements(By.XPath("html/body/div[2]/div[1]/ul[2]/li/a"));
            }
        }

        public int SectionListCount
        {
            get
            {
                return SectionList.Count;
            }
        }

        public string CurrentUserName
        {
            get
            {
                return CurrentUser.Text;
            }
        }

        public void ClickOnCreateNewTaskButton()
        {
            CreateNewTaskButton.Click();
        }

        public void FillInSearchInputAndPressEnter(string searchedText)
        {
            FillInInput(SearchInput, searchedText);
            SearchInput.SendKeys(Keys.Return);
        }

        public void ClickOnLogoutButton()
        {
            LogoutButton.Click();
        }

        public void SelectDesiredSectionAndClickOnIt(string sectionTitle)
        {
            var searchSection = SectionList.FirstOrDefault(x => x.Text.ToUpper() == sectionTitle.ToUpper())
                ?? throw new NoSuchElementException($"No such section found: {sectionTitle}");
            searchSection.Click();
        }
    }
}
