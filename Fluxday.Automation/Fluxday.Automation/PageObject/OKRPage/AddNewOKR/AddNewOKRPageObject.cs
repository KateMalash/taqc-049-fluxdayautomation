﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Fluxday.Automation.Tests.PageObject.OKRPage.AddNewOKR
{
    public class AddNewOKRPageObject : BaseObject.BasePageObject
    {
        public AddNewOKRPageObject(IWebDriver driver) : base(driver) { }

        protected IWebElement CancelButton
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='new_okr']/div[3]/div[2]/a/span"));
            }
        }

        protected IWebElement SaveButton
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='new_okr']/div[3]/div[2]/input"));
            }
        }

        protected IWebElement NameInput
        {
            get
            {
                return Driver.FindElement(By.Id("okr_name"));
            }
        }

        protected IWebElement StartDateInput
        {
            get
            {
                return Driver.FindElement(By.Id("okr_start_date"));
            }
        }

        protected IWebElement EndDateInput
        {
            get
            {
                return Driver.FindElement(By.Id("okr_end_date"));
            }
        }

        protected IReadOnlyCollection<IWebElement> AllObjectivesDivs
        {
            get
            {
                return Driver.FindElements(By.ClassName("objective-set"));
            }
        }
        protected IWebElement AddNewObjectiveButton
        {
            get
            {
                return Driver.FindElement(By.ClassName("add_fields"));
            }
        }

        public List<ObjectiveComponent> GetAllObjectives()
        {
             var objectives = new List<ObjectiveComponent>();
             foreach (var webElement in AllObjectivesDivs)
             {
                 objectives.Add(new ObjectiveComponent(webElement));
             }
             return objectives;
        }

        public ObjectiveComponent ClickOnAddNewObjectiveButton()
        {
            AddNewObjectiveButton.Click();
            return GetAllObjectives().Last();
        }

        public List<ObjectiveComponent> DeleteObjectiveByIndex(int indexOfObjective)
        {
            var allObjectives = GetAllObjectives();
            if (indexOfObjective >= 0 && indexOfObjective < allObjectives.Count && allObjectives.Count != 0)
            {
                allObjectives[indexOfObjective].ClickOnDeleteObjectiveButton();
                return GetAllObjectives();
            }
            else
            {
                throw new IndexOutOfRangeException($"Index {indexOfObjective} is out of range");
            }
        }  
      
        public void SetOKRByIndexOfObjective(int indexOfObjective,
                                             string nameOKR,
                                             string startDate,
                                             string endDate,
                                             string objectiveName,
                                             string[] keyResultsNames)
        {
            FillInInput(NameInput, nameOKR);
            FillInInput(StartDateInput, startDate);
            FillInInput(EndDateInput, endDate);
            var objectiveComponents = GetAllObjectives();
            objectiveComponents[indexOfObjective].SetObjective(objectiveName, keyResultsNames);
        }

        public void CancelButtonClick()
        {
            CancelButton.Click();
        }

        public void SaveButtonClick()
        {
            SaveButton.Click();
        }
    }
}
