﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Linq;

namespace Fluxday.Automation.Tests.PageObject.OKRPage.ViewOKRPage
{
    public class AdminViewOKRPage: ViewOKRPage
    {
        public AdminViewOKRPage(IWebDriver driver) : base(driver) { }

        protected IWebElement ChoiceTeamMemberButton
        {
            get
            {
                return Driver.FindElement(By.ClassName("select2-chosen"));
            }
        }

        protected IWebElement InputOfDropDownForTeamMembersChange
        {
            get
            {
                return Driver.FindElement(By.ClassName("select2-input"));
            }
        }

        protected IReadOnlyCollection<IWebElement> AllDropDownItems
        {
            get
            {
                return Driver.FindElements(By.XPath("//*[@id='okr_user_id']/child :: option"));
            }
        }

        protected IWebElement SelectOfDropDown
        {
            get
            {
                return Driver.FindElement(By.Id("okr_user_id"));
            }
        }

        public void ClickChoiceTeamMemberButton()
        {
            ChoiceTeamMemberButton.Click();
        }

        public int AllTeamMembersDropDownCount()
        {
            return AllDropDownItems.Count;
        }

        public IEnumerable<string> GetAllDropDownItemsTextValues()
        {
            var allDropDownItemsTextValues = AllDropDownItems.Select(a => a.Text);
            return allDropDownItemsTextValues;
        }

        public void SetInputOfDropDownForTeamMemberChange(string teamMember)
        {
            FillInInput(InputOfDropDownForTeamMembersChange, teamMember);
        }

        public void SelectDropDownItem(string dropDownItemValue)
        {
            var dropDownItemSelected = new SelectElement(SelectOfDropDown);
            dropDownItemSelected.SelectByText(dropDownItemValue);
        }
    }
}
