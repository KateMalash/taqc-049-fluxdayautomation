﻿using OpenQA.Selenium;

namespace Fluxday.Automation.Tests.PageObject.Users
{
    public class AddUserPage : EditUserPage
    {
        public AddUserPage(IWebDriver driver) : base(driver)
        {
        }

        protected IWebElement PasswordField
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='user_password']"));
            }
        }

        protected IWebElement ConfirmPasswordField
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='user_password_confirmation']"));
            }
        }

        public void FillInAddUserForm(string name, string nickName,
                                      string email, string employeeCode,
                                      string role, string password,
                                      string confirmPassword, string manager)
        {
            FillInName(name);
            FillInNickName(nickName);
            FillInEmail(email);
            FillInEmployeeCode(employeeCode);
            FillInRole(role);
            FillInPassword(password);
            FillInConfirmPassword(confirmPassword);
            FillInManager(manager);
        }

        public void FillInPassword(string password)
        {
            FillInInput(PasswordField, password);
        }

        public void FillInConfirmPassword(string confirmPassword)
        {
            FillInInput(ConfirmPasswordField, confirmPassword);
        }
    }
}
