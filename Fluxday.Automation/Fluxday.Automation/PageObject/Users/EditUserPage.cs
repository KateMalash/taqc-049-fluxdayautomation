﻿using OpenQA.Selenium;
using Fluxday.Automation.Tests.PageObject.BaseObject;

namespace Fluxday.Automation.Tests.PageObject.Users
{
    public class EditUserPage : BasePageObject
    {
        public EditUserPage(IWebDriver driver) : base(driver)
        {
        }

        protected IWebElement PageTitle
        {
            get
            {
                return Driver.FindElement(By.CssSelector("div.small-12.columns.form-action-up > div.title"));
            }
        }

        public bool IsPageTitlePresent
        {
            get
            {
                return IsElementPresent(By.CssSelector("div.small-12.columns.form-action-up > div.title"));
            }
        }

        protected IWebElement UserNameField
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='user_name']"));
            }
        }

        protected IWebElement NickNameField
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='user_nickname']"));
            }
        }

        protected IWebElement EmailField
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='user_email']"));
            }
        }

        protected IWebElement EmployeeCodeField
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='user_employee_code']"));
            }
        }

        protected IWebElement RoleField
        {
            get
            {
                return Driver.FindElement(By.CssSelector("#s2id_user_role"));
            }
        }

        protected IWebElement RoleFieldInput
        {
            get
            {
                return Driver.FindElement(By.CssSelector(".select2-drop > div > input"));
            }
        }

        protected IWebElement ManagersField
        {
            get
            {
                return Driver.FindElement(By.CssSelector("ul.select2-choices"));
            }
        }

        protected IWebElement SaveButton
        {
            get
            {
                return Driver.FindElement(By.CssSelector("div.small-12.columns.form-action-up > div.right > input"));
            }
        }

        protected IWebElement CancelButton
        {
            get
            {
                return Driver.FindElement(By.CssSelector("div.small-12.columns.form-action-up > div.right > a"));
            }
        }

        public void FillInEditUserForm(string name, string nickName,
                                       string email, string employeeCode,
                                       string role, string manager)
        {
            FillInName(name);
            FillInNickName(nickName);
            FillInEmail(email);
            FillInEmployeeCode(employeeCode);
            FillInRole(role);
            FillInManager(manager);
        }

        public void FillInName(string name)
        {
            FillInInput(UserNameField, name);
        }

        public void FillInNickName(string nickName)
        {
            FillInInput(NickNameField, nickName);
        }

        public void FillInEmail(string email)
        {
            FillInInput(EmailField, email);
        }

        public void FillInEmployeeCode(string employeeCode)
        {
            FillInInput(EmployeeCodeField, employeeCode);
        }

        public void FillInRole(string role)
        {
            RoleField.Click();
            RoleFieldInput.SendKeys(role);
            RoleFieldInput.SendKeys(Keys.Return);
        }

        public ManagersListComponent ClickOnManagersFieldToGetManagersList()
        {
            ManagersField.Click();
            return new ManagersListComponent(Driver);
        }

        public void FillInManager(string manager)
        {
            ManagersListComponent managersList = ClickOnManagersFieldToGetManagersList();
            managersList.SelectManager(manager);
        }

        public void ClickOnSaveButton()
        {
            SaveButton.Click();
        }

        public void ClickOnCancelButton()
        {
            CancelButton.Click();
        }
    }
}
