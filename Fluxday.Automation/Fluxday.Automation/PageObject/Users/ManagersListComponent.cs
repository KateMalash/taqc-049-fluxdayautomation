﻿using OpenQA.Selenium;
using Fluxday.Automation.Tests.PageObject.BaseObject;
using System.Collections.Generic;
using System.Linq;

namespace Fluxday.Automation.Tests.PageObject.Users
{
    public class ManagersListComponent : BasePageObject
    {
        public ManagersListComponent(IWebDriver driver) : base(driver)
        {
        }

        protected IReadOnlyCollection<IWebElement> ManagersList
        {
            get
            {
                return this.Driver.FindElements(By.CssSelector(".select2-result-label"));
            }
        }

        public void SelectManager(string manager)
        {
            var desiredManager = ManagersList.FirstOrDefault(x => x.Text.ToUpper() == manager.ToUpper())
                ?? throw new NoSuchElementException($"No such manager found:{manager}");
            desiredManager.Click();
        }
    }
}
