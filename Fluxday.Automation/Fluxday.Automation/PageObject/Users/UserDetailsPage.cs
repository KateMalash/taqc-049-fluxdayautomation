﻿using OpenQA.Selenium;
using Fluxday.Automation.Tests.PageObject.BaseObject;

namespace Fluxday.Automation.Tests.PageObject.Users
{
    public class UserDetailsPage : BasePageObject
    {
        public UserDetailsPage(IWebDriver driver) : base(driver)
        {
        }

        protected IWebElement UserNameLabel
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='pane3']/div/div[1]/div[4]/div"));
            }
        }

        protected IWebElement MenuGearButton
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='pane3']/div/div[1]/div[2]/a/div"));
            }
        }

        protected IWebElement MenuItemEdit
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='drop1']/li[1]/a"));
            }
        }

        protected IWebElement MenuItemChangePassword
        {
            get
            {
                return Driver.FindElement(By.LinkText("Change password"));
            }
        }

        protected IWebElement MenuItemDelete
        {
            get
            {
                return Driver.FindElement(By.LinkText("Delete"));
            }
        }

        public string CurrentDatailsPageUserName
        {
            get
            {
                return UserNameLabel.Text;
            }
        }

        public bool IsTheProperUserDetailsPage(string userName)
        {
                return UserNameLabel.Text == userName;
        }

        public void ClickOnEditMenuItem()
        {
            MenuGearButton.Click();
            MenuItemEdit.Click();
        }

        public void ClickOnChangePasswordMenuItem()
        {
            MenuGearButton.Click();
            MenuItemChangePassword.Click();
        }

        public void ClickOnDeleteMenuItemWithConfirmation()
        {
            MenuGearButton.Click();
            MenuItemDelete.Click();
            Driver.SwitchTo().Alert().Accept();
        }
    }
}
