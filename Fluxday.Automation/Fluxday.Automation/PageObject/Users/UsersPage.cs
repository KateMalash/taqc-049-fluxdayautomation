﻿using OpenQA.Selenium;
using Fluxday.Automation.Tests.PageObject.BaseObject;
using System.Collections.Generic;
using System.Linq;

namespace Fluxday.Automation.Tests.PageObject.Users
{
    public class UsersPage : BasePageObject 
    {
        public UsersPage(IWebDriver driver) : base(driver)
        {
        }

        protected IWebElement AddUser
        {
            get
            {
                return Driver.FindElement(By.LinkText("Add user"));
            }
        }

        protected IWebElement UsersListTitle
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='pane2']/div[1]/div"));
            }
        }

        public bool IsUserListTitlePresent
        {
            get
            {
                return IsElementPresent(By.XPath("//*[@id='pane2']/div[1]/div"));
            }
        }

        protected IReadOnlyCollection<IWebElement> ListOfUsers
        {
            get
            {
                return Driver.FindElements(By.CssSelector(".pane2-content .name"));
            }
        }

        public void ClickOnAddUser()
        {
            AddUser.Click();
        }

        public void GoToUserDetails(string userName)
        {
            var searchUser = ListOfUsers.FirstOrDefault(x => x.Text.ToUpper() == userName.ToUpper())
                ?? throw new NoSuchElementException($"No such user found: {userName}");
            searchUser.Click();
        }
    }
}
